import java.util.Random;
import java.util.Scanner;


public class Adventure {
	static String[] descriptions = {
			"You are standing in a pink and fluffy sparkly room.", 
			"You are standing in a dark and omnious room.", 
			"You are standing in a green and small room.", 
			"You are standing in a foggy and mysterious room.", 
			"You are standing in a great room."
		};
	static String[] thingDescriptions = {
		"knife", 
		"gun", 
		"ammo", 
		"whip", 
		"flower", 
		"book", 
		"lamp", 
		"notebook",
		"food", 
		"bottle"
	};
	static Room startRoom;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		setUpGame();
		Player p = new Player(startRoom);
		// sette opp in/out - commans
		System.out.println("Hello and welcome to the Adventure Game!");
		System.out.println(startRoom.getDescription());
		//skriv ut info om mulige kommandoer
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine().toLowerCase();
		while(!input.equals("quit")){
			p.printThingsInRoom();
			if(input.equals("go north"))
				System.out.println(p.goNorth());
			else if(input.equals("go south"))
				System.out.println(p.goSouth());
			else if(input.equals("go west"))
				System.out.println(p.goWest());
			else if(input.equals("go east"))
				System.out.println(p.goEast());
			else if(input.equals("take"))
				System.out.println(p.pickUp(sc));
			else if(input.equals("inventory"))
				p.printBag();
			input = sc.nextLine().toLowerCase();
		}
	}

	
	private static void setUpGame(){
		// shuffle beskrivelser for å få tilfeldige
		descriptions = shuffle(descriptions);
		Room[] t = new Room[descriptions.length];
		
		//opprett nye rom med beskrivelser, og lagre de i en tabell
		for(int i = 0; i<descriptions.length; i++){
			t[i] = new Room(descriptions[i]);
		}
		
		// shuffle beskrivelser for å få tilfeldige
		thingDescriptions = shuffle(thingDescriptions);
		Stuff[] t2 = new Stuff[thingDescriptions.length];
		
		//lag ting med beskrivelser
		for(int i = 0; i<thingDescriptions.length; i++){
			t2[i] = new Stuff(thingDescriptions[i]);
		}
		
		Random r = new Random();
		
		//putt ting i tilfeldige rom
		for(int i=0; i<t2.length; i++){
			t[r.nextInt(t.length)].putThingInRoom(t2[i]);
		}
		
		
		//koble rommene sammen
		t[0].setRoomEast(t[1]);
		t[1].setRoomEast(t[2]);
		t[1].setRoomSouth(t[3]);
		t[3].setRoomEast(t[4]);
		t[4].setRoomNorth(t[2]);
		
		//velger tilfeldig startrom
		startRoom = t[r.nextInt(t.length)];
	}


	private static String[] shuffle(String[] descriptions2) {
		Random r = new Random();
		String[] returnTab = new String[descriptions2.length];
		for(int i = 0; i<returnTab.length; i++){
			int randomTall = r.nextInt(descriptions2.length);
			while(returnTab[randomTall] != null)
				randomTall = r.nextInt(descriptions2.length);
			returnTab[randomTall] = descriptions2[i];
		}
		return returnTab;
	}
	
}
