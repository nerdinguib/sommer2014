import java.util.ArrayList;
import java.util.Scanner;


public class Player {

	//Feltvariabler
	private Room currentRoom;
	private ArrayList<Stuff> bag;
	
	//konstruktør
	Player(Room r){
		currentRoom = r;
		bag = new ArrayList<Stuff>();
	}

	//metoder
	String pickUp(Scanner sc){
		if(currentRoom.isEmpty())
			return "The room is empty, nothing to pick up here.";
		else{
			System.out.println("Write the number of the thing you want to pick up.");
			int input = sc.nextInt();
			bag.add(currentRoom.removeThing(input));
			return "You picked up " + bag.get(bag.size()-1).getDescription();
		}
	}
	
	void printBag(){
		if(bag.isEmpty())
			System.out.println("The bag is empty.");
		else{
			System.out.println("In the bag:");
			for(int i=0; i<bag.size(); i++)
				System.out.println(i+": " + bag.get(i).getDescription());
		}
			
	}
	
	void printThingsInRoom(){
		if(currentRoom.isEmpty())
			System.out.println("The room is empty.");
		else{
			currentRoom.printThings();
		}
	}
	
	
	String goNorth(){
		if(currentRoom.hasRoomNorth()){
			currentRoom = currentRoom.getRoomNorth(); 
			return currentRoom.getDescription();
		}
		return "You banged your head against the wall! There is no room north.";
	}

	String goSouth(){
		if(currentRoom.hasRoomSouth()){
			currentRoom = currentRoom.getRoomSouth(); 
			return currentRoom.getDescription();
		}
		return "You backed into the wall. There is nothing to the south.";
	}

	String goEast(){
		if(currentRoom.hasRoomEast()){
			currentRoom = currentRoom.getRoomEast(); 
			return currentRoom.getDescription();
		}
		return "You break your face. Tough luck. No room to the east";
	}

	String goWest(){
		if(currentRoom.hasRoomWest()){
			currentRoom = currentRoom.getRoomWest(); 
			return currentRoom.getDescription();
		}
		return "Turn around. No room in the west.";
}

	String getCurrentDescription(){
		return currentRoom.getDescription();
	}
}
