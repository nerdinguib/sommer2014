import java.util.ArrayList;


public class Room {
	
	// Feltvariabler
	private String description = null;
	private Room roomNorth = null, roomSouth = null, roomEast = null, roomWest = null;
	private ArrayList<Stuff> things;	// litt som Stuff[]
	
	//konstruktør
	Room(String d){
		description = d;
		things = new ArrayList<Stuff>();
	}
	
	//metoder
	void putThingInRoom(Stuff t){
		things.add(t);
	}
	
	
	String getDescription(){
		return description;
	}

	Room getRoomNorth(){
		return roomNorth;
	}
	
	Room getRoomSouth(){
		return roomSouth;
	}
	
	Room getRoomEast(){
		return roomEast;
	}
	
	Room getRoomWest(){
		return roomWest;
	}
	
	/**
	 * Sjekk om dette rommet har et rom
	 * mot nord 
	 * 
	 * @return true dersom dette rommet har 
	 * 			et rom mot nord
	 * 			false ellers 
	 */
	boolean hasRoomNorth(){
		return roomNorth != null;
	}
	
	boolean hasRoomSouth(){
		return roomSouth != null;
	}
	
	boolean hasRoomWest(){
		return roomWest != null;
	}
	
	boolean hasRoomEast(){
		return roomEast != null;
	}
	
	void setRoomNorth(Room r){
		roomNorth = r;
		if(!r.hasRoomSouth())
			r.setRoomSouth(this);
	}
	
	void setRoomSouth(Room r){
		roomSouth = r;
		if(!r.hasRoomNorth())
			r.setRoomNorth(this);
	}
	
	void setRoomEast(Room r){
		roomEast = r;
		if(!r.hasRoomWest())
			r.setRoomWest(this);
	}
	
	void setRoomWest(Room r){
		roomWest = r;
		if(!r.hasRoomEast())
			r.setRoomEast(this);
		
	}

	public boolean isEmpty() {
		return things.isEmpty();
	}

	public void printThings() {
		System.out.println("In the room: ");
		for(int i = 0; i<things.size(); i++){
			System.out.println(i+": " + things.get(i).getDescription());
		}
		
	}

	public Stuff removeThing(int input) {
		// TODO Auto-generated method stub
		return things.remove(input);
	}
}
