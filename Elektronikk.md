# Oversikt over komponenter #

### Logikk ###

* 74HCT32 (x14): Quad OR gate: http://www.ti.com/product/sn74hct32
* 75HC595  (x5): 8-bit shift register w/tristate output: http://www.ti.com/product/sn74hc595
* 74HC373  (x5): 8-bit D-latch w/tristate out: http://www.ti.com/lit/ds/scls452a/scls452a.pdf
* 74LVC245A (x5): 8-bit transceiver (5V to 3.3V): http://www.ti.com/product/sn74lvc245a/
* 74HCT08 (x25): Quad AND gate: http://www.ti.com/product/sn74hct08
* 74HCT04 (x4): Hex inverter: http://www.ti.com/product/sn74hct04
* 74HCT138 (x3): 3-to-8 demultiplexer: http://www.ti.com/product/sn74hct138
* 74LS32 (x3): Quad OR Gate: http://www.ti.com/product/sn74ls32
* HET4093BP (x4): Quad Schmitt-triggered NAND gate: http://www.nxp.com/products/logic/schmitt_triggers/HEF4093BP.html

### Annet ###
* AS6C1008 (x2): 128k 55ns RAM, 3-5V: http://www.alliancememory.com/pdf/AS6C1008%20feb%202007.pdf
* AT28C64B (x2): 64k EEPROM: http://www.atmel.com/devices/AT28C64B.aspx
* NE555P (x4): Timer
* Z84C0010 (x2): Z80 CPU
* NTE6840: ACIA (asynkron kommunikasjonschip)

### Klokkekrystaller ###

* 1x 32,768 kHz
* 1x 10 MHz
* 2x 7,3728 MHz
* 1x 3,579545 MHz
